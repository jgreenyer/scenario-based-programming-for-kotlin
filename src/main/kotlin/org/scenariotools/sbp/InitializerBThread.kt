package org.scenariotools.sbp

import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.*
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

class InitializerBThread (vararg scenarioClasses : KClass<out Scenario>) : BThread("Initializer") {

    private val initialMessageEventSetToScenariosMap = HashMap<IEventSet, MutableSet<KClass<out Scenario>>>()

    init {
        for(scenarioClass in scenarioClasses){
            val scenario = initializeScenario(scenarioClass)
            if (scenario != null){
                var scenariosKClassesForInitialMessageSet = initialMessageEventSetToScenariosMap[scenario.initEvents()]
                if(scenariosKClassesForInitialMessageSet == null){
                    scenariosKClassesForInitialMessageSet = HashSet<KClass<out Scenario>>()
                    initialMessageEventSetToScenariosMap.put(scenario.initEvents(), scenariosKClassesForInitialMessageSet)
                }
                scenariosKClassesForInitialMessageSet.add(scenarioClass)
            }
        }
    }

    private fun initializeScenario(scenarioClass : KClass<out Scenario>) : Scenario? {
        val c = scenarioClass.primaryConstructor
        if (c != null && c.parameters.isEmpty())
            return c.call()
        else
            return null
    }

    override fun bThreadLogic() = launch {
        println("Starting $name")
        while (true){
            val currentMessageEvent = waitFor(ALLEVENTS) as MessageEvent
            val scenariosToAdd = HashSet<Scenario>()
            for(entry in initialMessageEventSetToScenariosMap){
                val e = entry.key
                if (e.contains(currentMessageEvent)){
                    val scenarioKClasses = entry.value
                    for (scenarioKClass in scenarioKClasses){
                        val scenario = initializeScenario(scenarioKClass)
                        if (scenario == null) continue
                        scenario.eventThatInitializedScenario = currentMessageEvent
                        scenario.lastEvent = currentMessageEvent
                        //println("Initializer Thread is adding scenario $scenarioKClass")
                        scenariosToAdd.add(scenario)
                    }
                }
            }
            addBThreads(scenariosToAdd)
        }
    }

    private fun getScenarioKClassesInitializedByEvent(e: Event): Set<KClass<out Scenario>> {
        for (mapEntry in initialMessageEventSetToScenariosMap){
            val keyEvent = mapEntry.key
            if (keyEvent.contains(e))
                return mapEntry.value
        }
        return setOf()
    }


}