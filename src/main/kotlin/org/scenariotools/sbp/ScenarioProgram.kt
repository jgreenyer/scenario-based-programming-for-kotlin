package org.scenariotools.sbp

import org.scenariotools.bpk.BProgram
import org.scenariotools.bpk.BThread
import org.scenariotools.bpk.Event
import org.scenariotools.bpk.IConcreteEventSet
import kotlin.reflect.KClass

class ScenarioProgram(val environmentClasses : Set<KClass<*>>, scenarioClasses : Set<KClass<out Scenario>>, vararg activeScenarios : Scenario) :
        BProgram(InitializerBThread(*scenarioClasses.toTypedArray()), *activeScenarios) {


    override fun selectEvent(selectableEvents: IConcreteEventSet): Event? {
        var selectedEvent= findFirstSystemEvent(selectableEvents)
        if (selectedEvent==null && !selectableEvents.isEmpty()) {
            selectedEvent = selectableEvents.first() as MessageEvent
        }
        if (selectedEvent!=null )
            println("### EXECUTING $selectedEvent")
        return selectedEvent
    }

    private fun findFirstSystemEvent(selectableEvents: IConcreteEventSet) : MessageEvent?{
        for (event in selectableEvents){
            if (event is MessageEvent && isSystemEvent(event)) return event
        }
        return null
    }

    private fun isSystemEvent(messageEvent: MessageEvent) : Boolean {
        return !(messageEvent.sender::class in environmentClasses)
    }
}