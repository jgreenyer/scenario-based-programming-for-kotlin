package org.scenariotools.sbp

import org.scenariotools.bpk.Event
import sun.plugin2.message.Message
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.cast


open class MessageEvent(val sender:Any, val receiver:Any, val messageType:KFunction<*>, vararg val parameters : Any) : Event(){

    override fun equals(other : Any?): Boolean {
        if (other is MessageEvent)
            return signatureEquals(other, sender, receiver, messageType)
                    && parameterValuesEqual(other)
        else
            return false
    }

    private fun parameterValuesEqual(otherMessageEvent : MessageEvent) : Boolean{
        if (parameters.size != otherMessageEvent.parameters.size)
            assert(false, {"There must not be different numbers of parameters"})
        for (i in 0..parameters.size-1)
            if (parameters.get(i) != otherMessageEvent.parameters.get(i)) return false
        return true
    }

    override fun toString(): String {
        val paramString = parameters.joinToString(separator = ", ", prefix = "(", postfix = ")")
        return sender.toString() + "->" + receiver.toString() + "." + messageType.name + paramString
    }

}



fun signatureEquals(messageEvent : MessageEvent, sender: Any, receiver: Any, messageType : Any) : Boolean {
    if (messageType != messageEvent.messageType)
        return false
    if (sender != messageEvent.sender)
        return false
    if (receiver != messageEvent.receiver)
        return false
    return true
}