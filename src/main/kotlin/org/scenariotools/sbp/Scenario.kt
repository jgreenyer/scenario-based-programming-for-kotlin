package org.scenariotools.sbp

import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.*
import kotlin.reflect.KClass
import kotlin.reflect.KFunction

enum class ScenarioKind {
    ASSUMPTION, GUARANTEE
}

abstract class Scenario(name : String, val scenarioKind: ScenarioKind) : BThread(name){

    abstract fun initEvents() : IEventSet

    var eventThatInitializedScenario : Event? = null
    var lastEvent : Event? = null

    protected val interruptingEvents = MutableNonConcreteEventSet()
    protected val blockedEvents = MutableNonConcreteEventSet()



    override fun bThreadLogic() = launch {
        mainScenario()
    }

    abstract suspend fun mainScenario()

    suspend fun request(messageEvent: MessageEvent) {
        // todo: Implement assumption scenario semantics
        val event = bSync(messageEvent, interruptingEvents, blockedEvents)
        if (interruptingEvents.contains(event)) {
            println("Scenario $name is interrupted by message event $event")
            terminate()
        }else
            lastEvent = event
    }

    suspend fun waitForMessages(waitedForEvents : IEventSet) : Event{
        // todo: Implement assumption scenario semantics
        val allWaitedForEvents = MutableNonConcreteEventSet()
        allWaitedForEvents.add(waitedForEvents)
        allWaitedForEvents.addAll(interruptingEvents)
        val event = bSync(NOEVENTS, allWaitedForEvents, blockedEvents)
        if (interruptingEvents.contains(event)) {
            println("Scenario $name is interrupted by message event $event")
            terminate()
        }else
            lastEvent = event
        return event
    }


    suspend fun requestMessage(sender:Any, receiver:Any, messageType:KFunction<*>, vararg parameters : Any)
        = request(MessageEvent(sender, receiver, messageType, *parameters))

    suspend fun waitForMessages(sender:Any, receiver:Any, messageType:KFunction<*>, vararg parameters : Any)
        = waitForMessages(SymbolicMessageEvent(sender, receiver, setOf(messageType), *parameters))

    suspend fun waitForMessages(senderType : KClass<*>, receiverType: KClass<*>, messageType:KFunction<*>, vararg parameters : Any)
            = waitForMessages(SymbolicMessageEvent(senderType, receiverType, setOf(messageType), *parameters))

    suspend fun waitForMessages(sender:Any, receiver:Any, messageTypes: Collection<KFunction<*>>, vararg parameters : Any)
            = waitForMessages(SymbolicMessageEvent(sender, receiver, messageTypes, *parameters))

    suspend fun waitForMessages(senderType : KClass<*>, receiverType: KClass<*>, messageTypes: Collection<KFunction<*>>, vararg parameters : Any)
            = waitForMessages(SymbolicMessageEvent(senderType, receiverType, messageTypes, *parameters))

    fun lastMessage() : MessageEvent?{
        if (lastEvent is MessageEvent)
            return lastEvent as MessageEvent
        else
            return null
    }

}