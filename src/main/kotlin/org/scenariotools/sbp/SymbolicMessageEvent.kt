package org.scenariotools.sbp

import org.scenariotools.bpk.Event
import org.scenariotools.bpk.IEventSet
import java.lang.UnsupportedOperationException
import kotlin.reflect.KClass
import kotlin.reflect.KFunction


class SymbolicMessageEvent(val sender:Any?, val receiver:Any?, val messageTypes: Collection<KFunction<*>>, vararg val parameters : Any) : IEventSet{

    constructor(sender : Any?, receiver: Any?, messageType: KFunction<*>, vararg parameters : Any) : this(sender, receiver, setOf(messageType), *parameters)

    constructor(messageTypes: Collection<KFunction<*>>, vararg parameters : Any) : this(null, null, messageTypes, *parameters)

    constructor(messageType: KFunction<*>, vararg parameters : Any) : this(null, null, setOf(messageType), *parameters)

    override fun contains(element: Event): Boolean {
        if (!(element is MessageEvent))
            return false
        if (!signatureUnifyable(element))
            return false
        for (i in 0 until parameters.size-1){
            val thisParamVal = parameters[i]
            val otherParamVal = element.parameters[i]
            if (thisParamVal == otherParamVal)
                continue
            when (thisParamVal){
                ANY -> {}
                is Collection<*> -> if (!thisParamVal.contains(otherParamVal)) return false
                is IntRange ->
                    if (!(otherParamVal is Int) || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }
                is LongRange ->
                    if (!(otherParamVal is Long) || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }

            }
        }

        return true
    }

    private fun signatureUnifyable(messageEvent: MessageEvent): Boolean {
        if (!messageTypes.contains(messageEvent.messageType))
            return false

        if (!unifiable(sender, messageEvent.sender))
            return false
        if (!unifiable(receiver, messageEvent.receiver))
            return false

        if(parameters.size != messageEvent.parameters.size)
            return false

        return true
    }

    private fun unifiable(symbolicObj : Any?, obj : Any) : Boolean {
        if (symbolicObj == null || symbolicObj == ANY)
            return true
        else if (symbolicObj is KClass<*>){ // obj unbound, then types must be unifiable
            return (symbolicObj.isInstance(obj))
        } else { // obj bound, then bound objects must be equal
            return  (symbolicObj == obj)
        }
    }

}

object ANY

object ANYMESSAGETYPE : Collection<KFunction<*>>{

    override val size = 1

    override fun contains(element: KFunction<*>) = true

    override fun containsAll(elements: Collection<KFunction<*>>) = true

    override fun isEmpty() = false

    override fun iterator(): Iterator<KFunction<*>> {
        return object : Iterator<KFunction<*>>{

            override fun hasNext(): Boolean {
                return false
            }

            override fun next(): KFunction<*> {
                throw UnsupportedOperationException()
            }
        }
    }
}