package org.scenariotools.sbp.examples.rovercontroller

import org.scenariotools.sbp.ScenarioProgram
import org.scenariotools.sbp.examples.rovercontroller.activeScenarios.RoverDriveInterfaceScenario
import org.scenariotools.sbp.examples.rovercontroller.activeScenarios.RoverTelemetryInterfaceScenario
import org.scenariotools.sbp.examples.rovercontroller.guarantees.Move
import org.scenariotools.sbp.examples.rovercontroller.guarantees.PredictLeaderPos
import org.scenariotools.sbp.examples.rovercontroller.guarantees.SetLRPower
import org.scenariotools.sbp.examples.rovercontroller.guarantees.Turn
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*

fun main (args:Array<String>) {

    val rover = SocketCommunicator()
    val ref = SocketCommunicator()
    val IP = "127.0.0.1"

    Connector().connect(rover, ref, IP)

    val scenarioProgram = ScenarioProgram(
            setOf(Sensors::class),
            setOf(
                    PredictLeaderPos::class,
                    Move::class,
                    Turn::class,
                    SetLRPower::class
            ),
            RoverTelemetryInterfaceScenario(rover, ref, IP),
            RoverDriveInterfaceScenario(rover, ref, IP)
    )

    scenarioProgram.runBProgram()

}

class Connector{

    fun connect(rover: SocketCommunicator, ref: SocketCommunicator, IP: String) {
        var refPort = 0
        var roverPort = 0
        try {
            //------ Load config
            val conf =
                    try{
                        println("reading config file outside Jar: ./Settings/config.txt")
                        readFileOutsideJar("./Settings/config.txt")
                    }catch (e:FileNotFoundException){
                        println("could not read config file outside Jar, reading config file in Jar")
                        readFileInJar("/org/scenariotools/sbp/examples/rovercontroller/config.txt")
                    }

            val conIt = conf.iterator()
            while (conIt.hasNext()) {
                val a = conIt.next()
                if (a.contains("=") == true) {
                    val b = a.substring(0, a.indexOf('='))
                    if (b == "controlPort")
                        roverPort = Integer.parseInt(a.substring(a.indexOf('=') + 1))
                    else if (b == "observationPort")
                        refPort = Integer.parseInt(a.substring(a.indexOf('=') + 1))
                }
            }
            rover.connectToServer(IP, roverPort)
            ref.connectToServer(IP, refPort)
        } catch (e: Exception) {
        }

    }

    private fun readFileOutsideJar(fileName: String): List<String> {
        val inputStream = FileInputStream(fileName)
        return readFileFromInputStream(inputStream)
    }

    private fun readFileInJar(fileName: String): List<String> {
        val inputStream = this.javaClass.getResourceAsStream(fileName)
        return readFileFromInputStream(inputStream)
    }

    private fun readFileFromInputStream(inputStream: InputStream): List<String> {
        val data = ArrayList<String>()
        val scanner: Scanner
        scanner = Scanner(inputStream)

        while (scanner.hasNext()) {
            data.add(scanner.next())
            println(data.last())
        }
        scanner.close()
        return data
    }

}
