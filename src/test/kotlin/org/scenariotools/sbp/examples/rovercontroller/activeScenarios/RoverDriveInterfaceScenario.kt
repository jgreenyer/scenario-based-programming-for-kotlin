package org.scenariotools.sbp.examples.rovercontroller.activeScenarios

import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.SymbolicMessageEvent
import org.scenariotools.sbp.ANY
import org.scenariotools.sbp.examples.rovercontroller.DriveAdapter
import org.scenariotools.sbp.examples.rovercontroller.SocketCommunicator


class RoverDriveInterfaceScenario(val rover: SocketCommunicator, val ref: SocketCommunicator, val IP: String) : Scenario("RoverDriveInterfaceScenario", ScenarioKind.ASSUMPTION) {

    override fun initEvents() = NOEVENTS

    override suspend fun mainScenario() {
        println("Starting $name")


        while(true){

            waitForMessages(SymbolicMessageEvent(DriveAdapter::setLRPower, ANY, ANY))

            val speedLeft = lastMessage()!!.parameters[0] as Int
            val speedRight = lastMessage()!!.parameters[1] as Int
//            println("LEFT  SPEED: $speedLeft")
//            println("RIGHT SPEED: $speedRight")

            rover.noReply("Rover,setLRPower($speedLeft,$speedRight)")

        }
    }

}