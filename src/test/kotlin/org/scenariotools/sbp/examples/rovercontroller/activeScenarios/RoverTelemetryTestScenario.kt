package org.scenariotools.sbp.examples.rovercontroller.activeScenarios

import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.examples.rovercontroller.*


class RoverTelemetryTestScenario() : Scenario("RoverTelemetryInterfaceScenario", ScenarioKind.ASSUMPTION) {

    override fun initEvents() = NOEVENTS

    override suspend fun mainScenario() {
        println("Starting $name")

        // leader veers off slightly to the left

        val rts = listOf<Telemetries>(
                Telemetries(leaderPos=Coordinate(long=0.0, lat=0.0), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=0.0, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.0, lat=0.0), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=0.0, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.998804867267609, lat=14.9448661804199), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.016604000565152, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.998804867267609, lat=14.9448661804199), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.016604000565152, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.998804867267609, lat=14.9448661804199), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.06524602884948, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.995065987110138, lat=14.9935073852539), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.06524602884948, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.995065987110138, lat=14.9935073852539), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.06524602884948, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.989756286144257, lat=15.0812997817993), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.153041279716989, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.989756286144257, lat=15.0812997817993), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.153041279716989, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.981258392333984, lat=15.193395614624), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.265145736815773, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.981258392333984, lat=15.193395614624), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.265145736815773, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.969949305057526, lat=15.3466691970825), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.418438349589193, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.969949305057526, lat=15.3466691970825), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.418438349589193, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.952815055847168, lat=15.5159378051758), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.587751992430873, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.952815055847168, lat=15.5159378051758), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.587751992430873, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.931438207626343, lat=15.7103757858276), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.782272708548037, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.931438207626343, lat=15.7103757858276), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.782272708548037, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.931438207626343, lat=15.7103757858276), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=14.782272708548037, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.901070117950439, lat=15.9477653503418), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.019829149864691, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.901070117950439, lat=15.9477653503418), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.019829149864691, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.865317046642303, lat=16.1900672912598), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.26239962235586, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.865317046642303, lat=16.1900672912598), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.548818218820047, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.817044913768768, lat=16.4760036468506), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.548818218820047, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.817044913768768, lat=16.4760036468506), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.548818218820047, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.762643814086914, lat=16.784610748291), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.858125432938872, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.762643814086914, lat=16.784610748291), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=15.858125432938872, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.691925644874573, lat=17.088077545166), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=16.162752289415458, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.614798426628113, lat=17.4080028533936), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=16.484242694975855, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.614798426628113, lat=17.4080028533936), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=16.484242694975855, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.51737505197525, lat=17.7715721130371), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=16.85022395135404, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.51737505197525, lat=17.7715721130371), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=16.85022395135404, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.413765519857407, lat=18.1511096954346), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=17.23282290428086, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.413765519857407, lat=18.1511096954346), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=17.23282290428086, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.285786122083664, lat=18.5141372680664), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=17.600373591141555, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.285786122083664, lat=18.5141372680664), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=17.600373591141555, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.285786122083664, lat=18.5141372680664), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.011784291251118, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.140560239553452, lat=18.9195289611816), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.011784291251118, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=0.140560239553452, lat=18.9195289611816), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.011784291251118, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.00676216185092926, lat=19.3001708984375), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.399474617625145, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.00676216185092926, lat=19.3001708984375), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.399474617625145, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.161170214414597, lat=19.6797485351563), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=18.787406244723247, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.161170214414597, lat=19.6797485351563), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=19.207834874005215, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.334890812635422, lat=20.0896530151367), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=19.207834874005215, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.334890812635422, lat=20.0896530151367), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=19.207834874005215, followerCompass=0.0395646803081036) ,
                Telemetries(leaderPos=Coordinate(long=-0.49954617023468, lat=20.4672660827637), followerPos=Coordinate(long=1.00003385543823, lat=0.928262233734131), distance=19.59646426439702, followerCompass=0.0395646803081036)
                )

        for(rt in rts){
            bSuspend()

            //delay(100)
            bResume()

            requestMessage(Sensors,PosPredictor, PosPredictor::telemetries, rt)
        }

        println("TEST DONE.")
    }


}