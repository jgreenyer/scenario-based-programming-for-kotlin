package org.scenariotools.sbp.examples.rovercontroller

import org.scenariotools.sbp.examples.rovercontroller.guarantees.MoveKind
import org.scenariotools.sbp.examples.rovercontroller.guarantees.TurnKind
import kotlin.reflect.KClass

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

object Sensors : NamedElement("Sensors")

object PosPredictor : NamedElement("PosPredictor"){
    fun telemetries(rt:Telemetries){}
}

object FollowerCtrl : NamedElement("FollowerCtrl"){
    fun predictedTelemetries(rt:Telemetries){}
}

object DriveCmd : NamedElement("DriveCmd"){
    fun move(mk:MoveKind){}
    fun turn(tk: TurnKind){}
}

object DriveAdapter : NamedElement("DriveAdapter"){
    fun setLRPower(speedLeft:Int,speedRight:Int){}
}
