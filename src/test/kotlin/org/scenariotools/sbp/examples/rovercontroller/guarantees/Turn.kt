package org.scenariotools.sbp.examples.rovercontroller.guarantees

import org.scenariotools.sbp.*
import org.scenariotools.sbp.examples.rovercontroller.*

class Turn() : Scenario("Turn", ScenarioKind.GUARANTEE) {


    override fun initEvents() = SymbolicMessageEvent(PosPredictor,FollowerCtrl, FollowerCtrl::predictedTelemetries, ANY)

    override suspend fun mainScenario() {
        val t = (lastEvent as MessageEvent).parameters[0] as Telemetries
        val relativeAngle = relativeAngle(t)

        //println("relativeAngle: " + relativeAngle)

        blockedEvents.add(SymbolicMessageEvent(FollowerCtrl, DriveCmd, DriveCmd::turn, ANY))
        waitForMessages(FollowerCtrl, DriveCmd, DriveCmd::move, ANY)
        blockedEvents.clear()
        interruptingEvents

        if (relativeAngle > 10 && relativeAngle < 180 ) // turn left
            requestMessage(FollowerCtrl,DriveCmd,DriveCmd::turn, TurnKind.LEFT)
        else if (relativeAngle > 180 && relativeAngle < 350 ) // turn right
            requestMessage(FollowerCtrl,DriveCmd,DriveCmd::turn, TurnKind.RIGHT)
        else
            requestMessage(FollowerCtrl,DriveCmd,DriveCmd::turn, TurnKind.NONE)


    }

    private fun relativeAngle(rt: Telemetries) = ((((rt.followerCompass) - angle(rt)) + 360) % 360)
    private fun angle(rt: Telemetries) = angle(rt.followerPos, rt.leaderPos)
    private fun angle(c1 : Coordinate, c2 : Coordinate) = angle(c1.long, c1.lat, c2.long, c2.lat)
    private fun angle(x1 : Double, y1 : Double, x2 : Double, y2 : Double) = Math.toDegrees(Math.atan2(x2 - x1, y2 - y1))

}


enum class TurnKind {
    LEFT, RIGHT, NONE
}