package org.scenariotools.sbp.examples.rovercontroller.guarantees

import org.scenariotools.sbp.*
import org.scenariotools.sbp.examples.rovercontroller.DriveAdapter
import org.scenariotools.sbp.examples.rovercontroller.DriveCmd
import org.scenariotools.sbp.examples.rovercontroller.FollowerCtrl

class SetLRPower() : Scenario("SetLRPower", ScenarioKind.GUARANTEE) {


    override fun initEvents() = SymbolicMessageEvent(FollowerCtrl, DriveCmd, DriveCmd::move, ANY)

    override suspend fun mainScenario() {

        val mk = lastMessage()!!.parameters[0] as MoveKind

        waitForMessages(FollowerCtrl, DriveCmd, DriveCmd::turn, ANY)

        val tk = lastMessage()!!.parameters[0] as TurnKind

        val (speedLeft, speedRight) = computeLRSpeeds(mk, tk)

        requestMessage(DriveCmd, DriveAdapter, DriveAdapter::setLRPower, speedLeft, speedRight)

    }

    private fun computeLRSpeeds(mk : MoveKind, tk : TurnKind) : Pair<Int, Int>{
        var speedLeft = 0
        var speedRight = 0

        when (mk){
            MoveKind.STOP -> when (tk) {
                TurnKind.LEFT -> {
                    speedLeft = -50
                    speedRight = 50
                }
                TurnKind.RIGHT -> {
                    speedLeft = 50
                    speedRight = -50
                }
                TurnKind.NONE -> {
                    speedLeft = 0
                    speedRight = 0
                }
            }
            MoveKind.FWD -> when (tk) {
                TurnKind.LEFT -> {
                    speedLeft = -50
                    speedRight = 100
                }
                TurnKind.RIGHT -> {
                    speedLeft = 100
                    speedRight = -50
                }
                TurnKind.NONE -> {
                    speedLeft = 100
                    speedRight = 100
                }
            }
            MoveKind.BWD -> when (tk) {
                TurnKind.LEFT -> {
                    speedLeft = -100
                    speedRight = 50
                }
                TurnKind.RIGHT -> {
                    speedLeft = 50
                    speedRight = -100
                }
                TurnKind.NONE -> {
                    speedLeft = -100
                    speedRight = -100
                }
            }
        }
//        println("speedLeft: $speedLeft")
//        println("speedRight: $speedRight")

        return Pair<Int, Int>(speedLeft, speedRight)
    }



}


