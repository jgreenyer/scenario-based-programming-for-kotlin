package org.scenariotools.sbp.examples.rovercontroller.guarantees

import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.SymbolicMessageEvent
import org.scenariotools.sbp.ANY
import org.scenariotools.sbp.examples.rovercontroller.*

class PredictLeaderPos() : Scenario("PredictLeaderPos", ScenarioKind.GUARANTEE) {

    override fun initEvents() = SymbolicMessageEvent(Sensors,PosPredictor, PosPredictor::telemetries, ANY)

    override suspend fun mainScenario() {
        val t1 = lastMessage()!!.parameters[0] as Telemetries

        waitForMessages(Sensors,PosPredictor, PosPredictor::telemetries, ANY)

        val t2 = lastMessage()!!.parameters[0] as Telemetries

        requestMessage(PosPredictor, FollowerCtrl, FollowerCtrl::predictedTelemetries, predictTelemetries(t1,t2))

    }

    private val cyclesToPredictAhead = 5

    private fun predictTelemetries(rt1 : Telemetries, rt2 : Telemetries) : Telemetries{
        val predictedLeaderPos = predictPos(rt1.leaderPos, rt2.leaderPos)
        val predictedFollowerPos = predictPos(rt1.followerPos, rt2.followerPos)
        val predictedDistance = distance(predictedFollowerPos, predictedLeaderPos)

//        println("actual distance   : " +  rt2.distance)
//        println("predicted distance: " +  predictedDistance)

        return Telemetries(predictedLeaderPos,predictedFollowerPos,predictedDistance, rt2.followerCompass)
    }

    private fun predictPos(pos1 : Coordinate, pos2 : Coordinate) : Coordinate {
//        println("pos1             : " +  pos1)
//        println("pos2             : " +   pos2)
        val longOffset = pos2.long - pos1.long
        val latOffset = pos2.lat - pos1.lat
        val returnCoordinate = Coordinate(pos2.long+longOffset*cyclesToPredictAhead, pos2.lat+latOffset*cyclesToPredictAhead)
//        println("returnCoordinate : " +  returnCoordinate)
        return returnCoordinate
    }

    private fun distance (pos1 : Coordinate, pos2 : Coordinate) : Double {
        val longOffset = pos2.long - pos1.long
        val latOffset = pos2.lat - pos1.lat
        return Math.sqrt(longOffset*longOffset + latOffset*latOffset)
    }

}