package org.scenariotools.sbp.examples.rovercontroller.guarantees

import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.SymbolicMessageEvent
import org.scenariotools.sbp.ANY
import org.scenariotools.sbp.examples.rovercontroller.*

class Move() : Scenario("Move", ScenarioKind.GUARANTEE) {

    override fun initEvents() = SymbolicMessageEvent(PosPredictor,FollowerCtrl,FollowerCtrl::predictedTelemetries, ANY)

    override suspend fun mainScenario() {
        val t = lastMessage()!!.parameters[0] as Telemetries

        if(t.distance > 13){
            requestMessage(FollowerCtrl,DriveCmd, DriveCmd::move, MoveKind.FWD)
        } else if(t.distance < 12){
            requestMessage(FollowerCtrl,DriveCmd, DriveCmd::move, MoveKind.BWD)
        } else if(t.distance <= 13){
            requestMessage(FollowerCtrl,DriveCmd, DriveCmd::move, MoveKind.STOP)
        }
    }

}

enum class MoveKind {
    FWD, STOP, BWD
}