package org.scenariotools.sbp.examples.rovercontroller

import org.scenariotools.sbp.ScenarioProgram
import org.scenariotools.sbp.examples.rovercontroller.activeScenarios.RoverTelemetryTestScenario
import org.scenariotools.sbp.examples.rovercontroller.guarantees.Move
import org.scenariotools.sbp.examples.rovercontroller.guarantees.PredictLeaderPos
import org.scenariotools.sbp.examples.rovercontroller.guarantees.SetLRPower
import org.scenariotools.sbp.examples.rovercontroller.guarantees.Turn
import java.io.File
import java.io.FileNotFoundException
import java.util.*
import kotlin.system.measureTimeMillis

fun main (args:Array<String>) {


    val scenarioProgram = ScenarioProgram(
            setOf(Sensors::class),
            setOf(
                    PredictLeaderPos::class,
                    Move::class
                    ,
                    Turn::class,
                    SetLRPower::class
            ),
            RoverTelemetryTestScenario()
    )

    val timeElapsed = measureTimeMillis {
        scenarioProgram.runBProgram()
    }

    println("Time required: $timeElapsed ms")


}
