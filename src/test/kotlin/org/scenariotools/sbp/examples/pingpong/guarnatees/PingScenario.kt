package org.scenariotools.sbp.examples.pingpong.guarnatees

import org.scenariotools.sbp.*
import org.scenariotools.sbp.examples.pingpong.*

class PingScenario() : Scenario("PingScenario", ScenarioKind.GUARANTEE){

    override fun initEvents() = MessageEvent(Env, PongMe, PongMe::start)

    override suspend fun mainScenario() {
        println("$name requesting ping(1) ** starting :)")
        request(MessageEvent(PongMe, PingMe, PingMe::ping, 1))

        interruptingEvents.add(SymbolicMessageEvent(PingMe, PongMe, PongMe::pong, 101..200, ANY))

        while (true){
            waitForMessages(SymbolicMessageEvent(PingMe, PongMe, PongMe::pong, 1..100, ANY))
            val i = ((lastMessage()!!.parameters[0] as Int) + (lastMessage()!!.parameters[1]) as Int)
            println("$name requesting ping($i)")
            request(MessageEvent(PongMe, PingMe, PingMe::ping, i))
        }

    }


}