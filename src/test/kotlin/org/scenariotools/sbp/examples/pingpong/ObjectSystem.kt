package org.scenariotools.sbp.examples.pingpong

import kotlin.reflect.jvm.internal.impl.descriptors.Named

//data class Environment(val name : String)
//data class TableSensor(val name : String)
//data class ArmA(val name : String)
//data class ArmB(val name : String)

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

object Env : NamedElement("Env") {
}

object PingMe : NamedElement("PingMe") {
    fun ping(x : Int){}
}

object PongMe : NamedElement("PongMe")  {
    fun start(){}
    fun pong(x : Int, y : Int){}
}