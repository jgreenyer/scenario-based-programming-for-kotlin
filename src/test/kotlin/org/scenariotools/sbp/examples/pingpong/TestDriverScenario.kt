package org.scenariotools.sbp.examples.pingpong

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.BThread
import org.scenariotools.bpk.IEventSet
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.MessageEvent
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind

class TestDriverScenario : Scenario("TestDriverScenario", ScenarioKind.ASSUMPTION) {

    override fun initEvents() = NOEVENTS

    override suspend fun mainScenario() {
        println("Starting $name")
        println("requesting start")
        request(MessageEvent(Env, PongMe, PongMe::start))
    }


}