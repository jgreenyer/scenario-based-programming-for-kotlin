package org.scenariotools.sbp.examples.pingpong

import org.scenariotools.sbp.ScenarioProgram
import org.scenariotools.sbp.examples.pingpong.guarnatees.PingScenario
import org.scenariotools.sbp.examples.pingpong.guarnatees.PongScenario
import org.scenariotools.sbp.examples.productioncell.assumptions.ArmAArrivesAtPress
import org.scenariotools.sbp.examples.productioncell.assumptions.ArmAArrivesAtTable
import org.scenariotools.sbp.examples.productioncell.guarnatees.PickUpOnBlankArrived
import org.scenariotools.sbp.examples.productioncell.guarnatees.ReleaseAtPress


fun main (args:Array<String>) {

    val scenarioProgram = ScenarioProgram(
            setOf(Env::class),
            setOf(
                    PingScenario::class,
                    PongScenario::class
            ),
            TestDriverScenario())

    scenarioProgram.runBProgram()

}