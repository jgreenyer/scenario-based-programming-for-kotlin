package org.scenariotools.sbp.examples.pingpong.guarnatees

import org.scenariotools.sbp.MessageEvent
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.SymbolicMessageEvent
import org.scenariotools.sbp.examples.pingpong.*

class PongScenario() : Scenario("PongScenario", ScenarioKind.GUARANTEE){

    override fun initEvents() = SymbolicMessageEvent(PongMe, PingMe, PingMe::ping, 1..200)

    override suspend fun mainScenario() {
        val i = (lastMessage()!!.parameters[0] as Int)
        println("$name requesting pong($i, ${i+1})")
        request(MessageEvent(PingMe, PongMe, PongMe::pong, i, i+1))
        println("$name terminating")
    }


}