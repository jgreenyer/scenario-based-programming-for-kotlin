package org.scenariotools.sbp.examples.productioncell

import kotlin.reflect.jvm.internal.impl.descriptors.Named

abstract class NamedElement(val name : String){
    override fun toString(): String {
        return name
    }
}

object TableSensor : NamedElement("tableSensor") {
}

object ArmA : NamedElement("armA") {
    fun moveToPress(){}
    fun moveToTable(){}
    fun pickUp(){}
    fun release(){}
}

object ArmB : NamedElement("armB")  {
    fun pickUp(){}
    fun release(){}
    fun moveToPress(){}
    fun moveToDepositBelt(){}
}

object Press : NamedElement("press") {
    fun press(){}
}

object Controller : NamedElement("controller") {
    fun blankArrived(){}
    fun arrivedAtPress(){}
    fun arrivedAtTable(){}
    fun arrivedAtDepositBelt(){}
}