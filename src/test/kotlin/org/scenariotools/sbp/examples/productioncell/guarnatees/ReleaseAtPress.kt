package org.scenariotools.sbp.examples.productioncell.guarnatees

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.examples.productioncell.arrivedAtPress
import org.scenariotools.sbp.examples.productioncell.moveToTable
import org.scenariotools.sbp.examples.productioncell.release

class ReleaseAtPress() : Scenario("ReleaseAtPress", ScenarioKind.GUARANTEE){


    override fun initEvents() = arrivedAtPress

    override suspend fun mainScenario() {

        println("$name requesting release")
        request(release)
        println("$name requesting moveToTable")
        request(moveToTable)
        println("$name terminating")
    }


}