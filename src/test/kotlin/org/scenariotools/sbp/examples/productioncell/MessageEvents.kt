package org.scenariotools.sbp.examples.productioncell

import org.scenariotools.sbp.MessageEvent

val blankArrived = MessageEvent(TableSensor, Controller, Controller::blankArrived)
val pickUp = MessageEvent(Controller, ArmA, ArmA::pickUp)
val moveToPress = MessageEvent(Controller, ArmA, ArmA::moveToPress)
val arrivedAtPress = MessageEvent(ArmA, Controller, Controller::arrivedAtPress)
val release = MessageEvent(Controller, ArmA, ArmA::release)
val moveToTable = MessageEvent(Controller, ArmA, ArmA::moveToTable)
val arrivedAtTable = MessageEvent(ArmA, Controller, Controller::arrivedAtTable)
