package org.scenariotools.sbp.examples.productioncell.assumptions

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.examples.productioncell.arrivedAtTable
import org.scenariotools.sbp.examples.productioncell.moveToTable

class ArmAArrivesAtTable() : Scenario("ArmAArrivesAtTable", ScenarioKind.ASSUMPTION){

    override fun initEvents() = moveToTable

    override suspend fun mainScenario() {

        bSuspend(2000, NOEVENTS,"ArmA moving to table")

        println("$name requesting arrivedAtTable")
        request(arrivedAtTable)
        println("$name terminating")
    }


}