package org.scenariotools.sbp.examples.productioncell.guarnatees

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.examples.productioncell.blankArrived
import org.scenariotools.sbp.examples.productioncell.moveToPress
import org.scenariotools.sbp.examples.productioncell.pickUp

class PickUpOnBlankArrived() : Scenario("PickUpOnBlankArrived", ScenarioKind.GUARANTEE){


    override fun initEvents() = blankArrived

    override suspend fun mainScenario() {

        println("$name requesting pickUp")
        request(pickUp)
        println("$name requesting moveToPress")
        request(moveToPress)
        println("$name terminating")
    }


}