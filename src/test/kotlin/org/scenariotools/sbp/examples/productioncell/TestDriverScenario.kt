package org.scenariotools.sbp.examples.productioncell

import kotlinx.coroutines.experimental.launch
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind

class TestDriverScenario : Scenario("TestDriverScenario", ScenarioKind.ASSUMPTION) {

    override fun initEvents() = NOEVENTS

    override suspend fun mainScenario() {
        println("Starting $name")
        println("requesting blankArrived")
        request(blankArrived)

        bSuspend(5000, NOEVENTS,"Next blank approaching")

        println("requesting blankArrived")
        request(blankArrived)
    }


}