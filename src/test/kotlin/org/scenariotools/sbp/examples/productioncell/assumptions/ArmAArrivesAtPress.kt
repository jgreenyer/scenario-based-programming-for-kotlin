package org.scenariotools.sbp.examples.productioncell.assumptions

import kotlinx.coroutines.experimental.*
import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.sbp.Scenario
import org.scenariotools.sbp.ScenarioKind
import org.scenariotools.sbp.examples.productioncell.arrivedAtPress
import org.scenariotools.sbp.examples.productioncell.moveToPress

class ArmAArrivesAtPress() : Scenario("ArmAArrivesAtPress", ScenarioKind.ASSUMPTION){

    override fun initEvents()= moveToPress

    override suspend fun mainScenario() {

        bSuspend(2000, NOEVENTS, "ArmA moving to press")

        println("$name requesting arrivedAtPress")
        request(arrivedAtPress)
        println("$name terminating")
    }



}