package org.scenariotools.sbp.examples.productioncell

import org.scenariotools.sbp.ScenarioProgram
import org.scenariotools.sbp.examples.productioncell.assumptions.ArmAArrivesAtPress
import org.scenariotools.sbp.examples.productioncell.assumptions.ArmAArrivesAtTable
import org.scenariotools.sbp.examples.productioncell.guarnatees.PickUpOnBlankArrived
import org.scenariotools.sbp.examples.productioncell.guarnatees.ReleaseAtPress


fun main (args:Array<String>) {

    val scenarioProgram = ScenarioProgram(
            setOf(TableSensor::class),
            setOf(
                    PickUpOnBlankArrived::class,
                    ReleaseAtPress::class,
                    ArmAArrivesAtPress::class,
                    ArmAArrivesAtTable::class
                    ),
    TestDriverScenario())

    scenarioProgram.runBProgram()

//    val initializerBThread = InitializerBThread(PingScenario::class)
//
//    val bProgram = BProgram(TestDriverScenario(), initializerBThread)
//    bProgram.runBProgram()

}